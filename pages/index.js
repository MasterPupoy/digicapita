import Head from 'next/head';
import { Container } from 'react-bootstrap';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';

import Navigation from '../components/Navigation';

export default function Home() {
  return (
    <div>
      <Head>
        <title>Digicapita</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <>
          <Navigation />
        </>
      </main>
    </div>
  )
}
