import Head from "next/head";
import Login from "../../components/LoginForm";
import { Container } from 'react-bootstrap';

import styles from '../../styles/Home.module.css';

export default function LoginPage() {
    
    return(
        <div>
            <Head>
                <title>Digicapita Login</title>
            </Head>
            <main>
                <Container>
                    <div className={styles.loginCard}>
                        <Login />
                    </div>
                </Container>
            </main>
        </div>
    );
}