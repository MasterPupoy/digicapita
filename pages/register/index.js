import Head from 'next/head';
import { Container } from 'react-bootstrap';

import Register from '../../components/RegisterForm'

import styles from '../../styles/Home.module.css';

export default function Signup() {

    return(

        <div>
            <Head>
                <title>Register - Digicapita</title>
            </Head>

            <main>
                <Container>
                    <Register />

                </Container>
            </main>
        </div>

    )

}