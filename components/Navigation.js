import styles from '../styles/Home.module.css';
import { Navbar, Nav, Button } from 'react-bootstrap';
import Link from 'next/link'

const Navigation = () => {

    return (
        <>
        <div className={styles.navigation}>
            <Navbar bg="transparent" expand="lg">
                <Navbar.Brand>DigiCapita</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbar-nav" />
                    <Navbar.Collapse id="navbar-nav">
                        <Nav className="ml-auto">
                            <Nav.Link className="pr-3" href="#">Home</Nav.Link>
                            <Nav.Link className="pr-3" href="#">What's New?</Nav.Link>                            
                        </Nav>
                        <Link href="/login">
                                <Button className={styles.login}>Log-in</Button>
                            </Link>
                            
                            <Link href="/register">  
                                <Button className={styles.signup}>Sign-up</Button>
                            </Link> 
                    </Navbar.Collapse>
            </Navbar>
        </div>
        </>
    )

}

export default Navigation