import React, { useEffect, useState } from 'react';
import { Form, Button, Card, Row, Col, Spinner } from 'react-bootstrap';
import Swal from 'sweetalert2';

import Router from 'next/router'; 

const Register = () => {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [mobile, setMobile] = useState('');
    const [isLoading, setLoading] = useState('');


    const ButtonLoad = () => {

        return(
            <Button 
                type='submit' 
                variant='primary'
                disabled={isLoading}
            >
                {isLoading ? <Spinner animation='grow' /> : 'Sign-up' }   
            </Button>
        )
    }   
    
    const signup = async (e) => {
        e.preventDefault(); // prevent default form behaviour on submit
        
        setLoading(true); // set loading to true for signup button 

        // verify email first before payload delivery

        let verifyEmail = await fetch(`${process.env.DB_HOST}/users/`, {
            method : 'POST',
            headers : {'Content-Type' : 'application/json'},
            body : JSON.stringify({
                
                emailAdd : email

            })
        })

        // read incoming response

        let verified = await verifyEmail.json();         
        
        // if email exists, throw error

        if(verified === true){            
            Swal.fire({
                icon : 'error',
                title : 'Email already exists...',
                titleText : 'Email exists',
                text : 'Please use a different email '
            })

            setLoading(false)
        }
        
        // if no email match, proceed to registration 
        
        if(verified === false){ 

            let payload = {     // payload params
                method: 'POST',
                headers: {'Content-Type' : 'application/json'},
                body : JSON.stringify({
                    fName : firstName,
                    lName : lastName,
                    emailAdd : email,
                    password : password,
                    mobile : mobile
                })
            };

            // post to register

            let response = await fetch(`${process.env.DB_HOST}/users/register`, payload);
            let registerSuccess = await response.json(); // return true if successful

            if (registerSuccess === true) { // swal alert on success
                Swal.fire({ 
                    icon:'success',
                    title:'Success!',
                    titleText:'Success!',
                    text:'You\'ve been successfully registered. Please login',
                })

                Router.push('/login'); // redirect to login page after registration
            }
        }
    }

    return (
        <div>
            <Row>
                <Col>
                    <Card>
                        <Card.Title>
                            Sign-up
                        </Card.Title>
                        <Form onSubmit={e => signup(e)}>
                            <Form.Group controlId='firstName'>
                                <Form.Label>First Name</Form.Label>
                                <Form.Control type='text' placeholder='Enter your first name' value={firstName} onChange={e => setFirstName(e.target.value)} required/>
                            </Form.Group>

                            <Form.Group controlId='lastName'>
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control type='text' placeholder='Enter your last Name' value={lastName} onChange={e => setLastName(e.target.value)} required/>
                            </Form.Group>
                            
                            <Form.Group controlId='email'>
                                <Form.Label>Email Address</Form.Label>
                                <Form.Control type='email' placeholder='Email Address' value={email} onChange={e => setEmail(e.target.value)} required/>
                            </Form.Group>

                            <Form.Group controlId='number'>
                                <Form.Label>Mobile Number</Form.Label>
                                <Form.Control type='text' placeholder='Mobile Number' value={mobile} onChange={e => setMobile(e.target.value)} required/>
                            </Form.Group>

                            <Form.Group controlId='password'>
                                <Form.Label>Password</Form.Label>
                                <Form.Control type='password' placeholder='Password' value={password} onChange={e => setPassword(e.target.value)} required/>
                            </Form.Group>

                            <Form.Group controlId='passwordConfirm'>
                                <Form.Label>Confirm Password</Form.Label>
                                <Form.Control type='password' placeholder='Retype password' value={passwordConfirm} onChange={e => setPasswordConfirm(e.target.value)} required/>
                            </Form.Group>

                            <ButtonLoad />
                        </Form>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}

export default Register