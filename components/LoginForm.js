import React, { useState, useEffect } from 'react';
import { Form, Button, Card, Row, Col }  from 'react-bootstrap';

const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    return(
        <div>
            <Row>
                <Col lg={{ span: 6, offset: 6}}>
                    <Card>
                        <Card.Body>
                            <Card.Title>
                                Log-in
                            </Card.Title>
                            <Form>
                                <Form.Group controlId="email">
                                    <Form.Label>Email Address</Form.Label>
                                    <Form.Control type="email" placeholder="Email" />
                                </Form.Group>

                                <Form.Group controlId="password">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="password" />
                                </Form.Group>

                                <Form.Group controld="checkbox">
                                    <Form.Check type="checkbox" label="Remember Me" />
                                </Form.Group>

                                <Button type="submit" variant="warning">Login</Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}

export default Login
